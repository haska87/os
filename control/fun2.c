float subdiv(int a, int b, int t) {
	if (t == 0) return a - b;
	else return (float) a / b;
}
