#include <stdio.h>

int sumul(int a, int b, int t);
float subdiv(int a, int b, int t);

void main() {
	int a, b;
	printf("enter \"a\" and \"b\": ");
	scanf("%i%i", &a, &b);
	
	printf("a + b = %i\n", sumul(a, b, 0));
	printf("a * b = %i\n", sumul(a, b, 1));
	printf("a - b = %i\n", (int) subdiv(a, b, 0));

	if (b == 0) printf("error: division by zero!\n");
	else printf("a / b = %f\n", subdiv(a, b, 1));
}
